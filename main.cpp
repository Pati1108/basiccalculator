#include <iostream>
using namespace std;

/**********************
 ZADANKO
 Napisz program realizuj�cy zadania kalkulatora w zakresie podstawowej arytmetyki.
 Program ma wy�wietli� u�ytkownikowi menu tekstowe, w kt�rym b�dzie mia� do wyboru
 5 podstawowych operacji. Po wybraniu opcji: jesli jest 0 to koniec dzia�ania,
 je�li 1 z opcji zosta�a wybrana program ma wczyta� l. do przeprowadzenia operacji
 i wy�wietli� wynik. Po obl wyniku program ma ponownie wyswietlic menu, w ktorym
 nalezy wybrac co robimy dalej. Przed implementacj� przygotuj wykres
 *******************/

void DisplayWelcome()
{
   cout << "Hello\n\n";
   cout << "Welcome in calculator\n";
}

void ShowActions ()
{
   cout << "=============================================\n";
   cout << "Which action do you want to perform?\n\n";
   cout << "1. Addition\n";
   cout << "2. Subtraction\n";
   cout << "3. Multiplication\n";
   cout << "4. Division\n";
   cout << "5. Modulus\n";
   cout << "0. End program\n\n";
   cout << "Choose action number: " << endl;
   cout << "=============================================\n";
}

void CalculateSumOfTwoNumbers(void)
   {
   int number_1, number_2;
   int result;
   cout << "choose first number: " << endl;
   cin >> number_1;
   cout << "choose second number: " << endl;
   cin >> number_2;
   result = number_1 + number_2;
   cout << "The result is: " << result << endl;
   }

void CalculateDifferenceOfTwoNumbers(void)
   {
   int number_1, number_2;
   int result;
   cout << "choose first number: " << endl;
   cin >> number_1;
   cout << "choose second number: " << endl;
   cin >> number_2;
   result = number_1 - number_2;
   cout << "The result is: " << result << endl;
   }

void CalculateRatioOfTwoNumbers(void)
   {
   int number_1, number_2;
   int result;
   cout << "choose first number: " << endl;
   cin >> number_1;
   cout << "choose second number: " << endl;
   cin >> number_2;
   result = number_1 * number_2;
   cout << "The result is: " << result << endl;
   }

void CalculateQuotientOfTwoNumbers (void)
   {
   int number_1, number_2;
   int result;
   cout << "choose first number: " << endl;
   cin >> number_1;
   cout << "choose second number: " << endl;
   cin >> number_2;
   result = number_1 / number_2;
   cout << "The result is: " << result << endl;
   }

void CalculateRestOfTwoNumbers (void)
   {
   int number_1, number_2;
   int result;
   cout << "choose first number: " << endl;
   cin >> number_1;
   cout << "choose second number: " << endl;
   cin >> number_2;
   result = number_1 % number_2;
   cout << "The result is: " << result << endl;
   }

void DisplayEnd ()
   {
   cout << "This is the end " << endl;
   cout << "Danke" << endl;
   }

int main()
   {

   int action;

   DisplayWelcome();
   while (1)
      {
      ShowActions();
      cin >> action;

      switch (action)
         {
         case 1:
            CalculateSumOfTwoNumbers();
            break;

         case 2:
            CalculateDifferenceOfTwoNumbers();
            break;

         case 3:
            CalculateRatioOfTwoNumbers();
            break;

         case 4:
            CalculateQuotientOfTwoNumbers();
            break;

         case 5:
            CalculateRestOfTwoNumbers();
            break;

         case 0:
            DisplayEnd();
            return 0;
            break;

         default:
            cout << "Wrong number" << endl;
            break;
         }
      }
   return 0;
   }
